// import logo from './logo.svg';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { fetchPost } from './redux/action/postAtion';

function App() {


  const dispatch = useDispatch();
  const state = useSelector(state => state.posts.data)
console.log("Test : ",state);
  useEffect(() => {
    dispatch(fetchPost())
  }, [])

  return (
    <div >
      <h1>All Tutorials</h1>
      <ul>

        {
          state.map(post => 
            <div key={post._id}>
              Tutorials : 
              <a href=""> {post.title}</a>
              
            </div>
          )
        }
      </ul>
    </div>
  );
}

export default App;
