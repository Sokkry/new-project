import { postReducer } from "../reducers/postReducers";
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger'

export const store = createStore (postReducer, applyMiddleware(thunk,logger))