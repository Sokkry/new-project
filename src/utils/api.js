import axios from 'axios';

const api = axios.create({
    baseURL: 'http://110.74.194.124:3034/api',
});
export default api;